package salestax;
import org.junit.jupiter.api.Test;
import salestax.dataObjects.Product;
import salestax.exceptions.ProductTypeNotExistsException;
import salestax.properties.SalesTaxProperties;

import static org.junit.Assert.*;


public class SalesTaxCalculatorTest {

    @Test
    public void shouldReturnNetSalesPriceForNonEssentialProduct() throws Exception {

        Product nonEssentialProduct = new Product("Music CD", 14.99, SalesTaxProperties.PRODUCT_TYPE_NON_ESSENTIAL);
        SalesTaxCalculator salesTaxCalculator = new SalesTaxCalculator(nonEssentialProduct);

        double netPrice = 0;
        try {
            netPrice = salesTaxCalculator.getNetPriceOfProduct();
        } catch (Exception e) {
           throw e;
        }
        assertEquals(16.49, netPrice,0.1);
    }

    @Test
    public void shouldReturnNetSalesPriceForEssentialProduct() throws Exception {

        Product essentialProduct = new Product("Book", 12.49, SalesTaxProperties.PRODUCT_TYPE_ESSENTIAL);
        SalesTaxCalculator salesTaxCalculator = new SalesTaxCalculator(essentialProduct);
        double netPrice = 0;
        try {
            netPrice = salesTaxCalculator.getNetPriceOfProduct();
        } catch (Exception e) {
            throw e;
        }
        assertEquals(12.49, netPrice,0.1);
    }

    @Test
    public void shouldReturnNetSalesPriceForImportedProduct() throws Exception {

        Product importedProduct = new Product("Victoria Secret", 27.99, SalesTaxProperties.PRODUCT_TYPE_IMPORTED);
        SalesTaxCalculator salesTaxCalculator = new SalesTaxCalculator(importedProduct);

        double netPrice = 0;
        try {
            netPrice = salesTaxCalculator.getNetPriceOfProduct();
        } catch (Exception e) {
            throw e;
        }
        assertEquals(32.9, netPrice,0.9);
    }

    @Test
    public void shouldReturnExceptionIfProductTypeDoesNotExists(){

        Product anyProduct = new Product("Asdf", 14.99, 4);
        SalesTaxCalculator salesTaxCalculator = new SalesTaxCalculator(anyProduct);

        Exception exception = assertThrows(ProductTypeNotExistsException.class, () -> {
            salesTaxCalculator.getNetPriceOfProduct();
        });
    }


}
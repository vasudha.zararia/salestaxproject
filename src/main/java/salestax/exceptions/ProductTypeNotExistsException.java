package salestax.exceptions;

public class ProductTypeNotExistsException extends Exception {
    private  int productType;
    public ProductTypeNotExistsException(int productType) {
        this.productType = productType;
    }

    @Override
    public String getMessage() {
        return "Product type does not exists:" + productType;
    }
}

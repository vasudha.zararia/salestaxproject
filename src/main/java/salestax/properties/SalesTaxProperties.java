package salestax.properties;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class SalesTaxProperties {

    //Product Type
    public static final  int PRODUCT_TYPE_ESSENTIAL = 1;
    public static final int PRODUCT_TYPE_NON_ESSENTIAL = 2;
    public static final int PRODUCT_TYPE_IMPORTED = 3;

    public static final ArrayList<Integer> PRODUCT_TYPE_LIST = new ArrayList<Integer>(Arrays.asList(PRODUCT_TYPE_ESSENTIAL, PRODUCT_TYPE_NON_ESSENTIAL, PRODUCT_TYPE_IMPORTED));

    //Sales tax percentage
    public static final float ESSENTIAL_PRODUCT_TAX_PERCENTAGE = 0.0f;
    public static final float NON_ESSENTIAL_PRODUCT_PERCENTAGE = 10f;
    public static final float IMPORTED_PRODUCT_TAX_PERCENTAGE = 15f;


}

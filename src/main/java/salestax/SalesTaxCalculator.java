package salestax;

import salestax.dataObjects.Product;
import salestax.exceptions.ProductTypeNotExistsException;
import salestax.properties.SalesTaxProperties;

public class SalesTaxCalculator {
    private Product product;

    public SalesTaxCalculator(Product product) {
        this.product = product;
    }

    public double getNetPriceOfProduct() throws ProductTypeNotExistsException {
       
        int productType =  product.getProductType();
        if(!checkIfProductExists(productType))
        {
            throw new ProductTypeNotExistsException(productType);
        }

        double netProductPrice = 0.0;
        switch(productType) {
            case SalesTaxProperties.PRODUCT_TYPE_NON_ESSENTIAL :
                netProductPrice = getNetPriceAfterTax(product.getProductPriceBeforeTax(), SalesTaxProperties.NON_ESSENTIAL_PRODUCT_PERCENTAGE);
                break;

            case SalesTaxProperties.PRODUCT_TYPE_ESSENTIAL :
                netProductPrice = getNetPriceAfterTax(product.getProductPriceBeforeTax(), SalesTaxProperties.ESSENTIAL_PRODUCT_TAX_PERCENTAGE);
                break;

            case SalesTaxProperties.PRODUCT_TYPE_IMPORTED :
                netProductPrice = getNetPriceAfterTax(product.getProductPriceBeforeTax(), SalesTaxProperties.IMPORTED_PRODUCT_TAX_PERCENTAGE);
                break;
        }
        return netProductPrice;
    }

    private boolean checkIfProductExists(int productType) {
       return SalesTaxProperties.PRODUCT_TYPE_LIST.contains(productType);
    }

    private double getNetPriceAfterTax(double priceBeforeTax, float percentageTax) {
        return priceBeforeTax + ( percentageTax * priceBeforeTax)/100;
    }
}

package salestax.dataObjects;

public class Product {

    private String productName;
    private double productPriceBeforeTax;
    private int productType;

    public Product(String productName, double productPriceBeforeTax, int productType) {
        this.productName = productName;
        this.productPriceBeforeTax = productPriceBeforeTax;
        this.productType = productType;
    }

    public int getProductType() {
        return productType;
    }
    public double getProductPriceBeforeTax() {
        return productPriceBeforeTax;
    }

}
